from setuptools import setup

setup(name='RSTableResizer',
      version='1.0',
      description='Python Distribution Utilities',
      author='Mateusz Celary',
      author_email='mateusz.celary@s2innovation.com',
      packages=['RSTableResizer'],
)
