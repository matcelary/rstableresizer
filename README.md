# RSTableResizer

Simple script to resize RST tables.

## Installation

pip install .

## Usage

import RSTableResizer as r
r.resize("filename.rst")
