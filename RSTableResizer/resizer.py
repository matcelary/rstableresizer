import sys
from io import open

def _get_table_width(table, reserve=4):
    # remove row separation lines
    max_length = -1
    for row in table:
        if row.startswith("|"):
            row = row.rstrip().rstrip("|").rstrip()
            if max_length < len(row):
                max_length = len(row)
    return max_length + reserve

def _reformat_line(line, width):
    """ Takes a table line and makes it a given size """
    # line with a break
    if line.startswith("+"):
        new_line = line.rstrip("+\n")
        sign = "=" if new_line.endswith("=") else "-"
        letters_to_add = sign * ( width - len(new_line) ) + "+"
    elif line.startswith("|"):
        new_line = line.rstrip().rstrip("|").rstrip()
        letters_to_add = " " * (width - len(new_line)) + "|"
    new_line = new_line + letters_to_add + "\n"
    return new_line


def resize(filename):
    # load file
    with open(filename, mode="r", encoding="utf-8") as f:
        lines = f.readlines()
    # find tables
    row = 0
    while row < len(lines):
        if lines[row].startswith("+-"):
            # table found
            table = []
            for r in range(row,len(lines)):
                if lines[r].startswith("+") or lines[r].startswith("|"):
                    table.append(lines[r])
                else:
                    break
            # find table size
            table_width = _get_table_width(table)
            table_length = len(table)
            # reformat table
            for r in range(row, row + table_length):
                lines[r] = _reformat_line(lines[r], table_width)
            # skip to the end of the table
            row += table_length
        else:
            row += 1
    with open(filename, mode="w", encoding="utf-8") as f:
        f.writelines(lines)


if __name__ == "__main__":
    resize_tables(sys.argv[1])


